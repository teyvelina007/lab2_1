package Lab2;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.IntField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Indexer {
    private IndexWriter indexWriter;
    private IndexWriterConfig indexWriterConfig;

    public Indexer(String indexDirectoryPath) {
        try {
            //this directory will contain the indexes
            Directory indexDirectory = FSDirectory.open(new File(indexDirectoryPath));
            //create the indexer
            Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_40);
            indexWriterConfig = new IndexWriterConfig(Version.LUCENE_40, analyzer);
            indexWriterConfig.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
            indexWriter = new IndexWriter(indexDirectory, indexWriterConfig);
        } catch (Exception e) {
            e.printStackTrace();
            //System.err.println("Error opening the index. " + e.getMessage());
        }
    }

    private void close() {
        try {
            indexWriter.commit();
            indexWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Document setDocument(Embroidery embroidery) {
        Document document = new Document();

        document.add(new StringField(LuceneConstants.URL, embroidery.getEmbroidery_url(), Field.Store.YES));
        document.add(new StringField(LuceneConstants.IMAGE, embroidery.getEmbroidery_image(), Field.Store.YES));
        document.add(new StringField(LuceneConstants.TITLE, embroidery.getEmbroidery_title(), Field.Store.YES));
        document.add(new IntField(LuceneConstants.PRICE, embroidery.getEmbroidery_price(), Field.Store.YES));
        document.add(new StringField(LuceneConstants.DESCRIPTION, embroidery.getEmbroidery_description(), Field.Store.YES));
        document.add(new StringField(LuceneConstants.SPECIFICATIONS, embroidery.getEmbroidery_specifications(), Field.Store.YES));
        document.add(new StringField(LuceneConstants.TYPE, embroidery.getEmbroidery_type(), Field.Store.YES));

        return document;
    }

    public void createIndex (ArrayList<Embroidery> list) {
        System.out.println("Indexing...");
        for(Embroidery embroidery: list) {
            try {
                indexWriter.addDocument(setDocument(embroidery));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        int numIndexed = 0;
        long startTime = System.currentTimeMillis();
        numIndexed = indexWriter.numDocs();
        long endTime = System.currentTimeMillis();
        System.out.println(numIndexed + " objects indexed, time taken: " + (endTime - startTime) + " ms");
        close();
    }
}

package Lab2;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Parser {
    public static String parseEmbroideryUrl(String elementHtmlCode, String substringbefore, String mainurl) {
        String urlpart = "";
        int position = elementHtmlCode.indexOf(substringbefore);
        position += substringbefore.length();
        while (elementHtmlCode.charAt(position) != '\"') {
            urlpart += elementHtmlCode.charAt(position);
            position++;
        }
        return mainurl + urlpart;
    }


    public static String parseImageUrl(String elementHtmlCode, String substringbefore) {
        String url = "";
        int position = elementHtmlCode.indexOf(substringbefore);
        position += substringbefore.length();
        while (elementHtmlCode.charAt(position) != '\"') {
            url += elementHtmlCode.charAt(position);
            position++;
        }
        return url;
    }


    public static String findValue(String elementHtmlCode, String substringbefore, String substringafter) {
        String value = "";
        int position = elementHtmlCode.indexOf(substringbefore);
        position += substringbefore.length();
        while (!elementHtmlCode.substring(position, substringafter.length() + position).equals(substringafter)) {
            //(elementHtmlCode.charAt(position) != '<'){
            value += elementHtmlCode.charAt(position);
            position++;
        }
        return value;
    }


    /*
    <div id="itemdesc" aria-labelledby="ui-id-1" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" aria-hidden="false">В набор входят: хлопковое мулине, 18 канва Аида белого цвета , игла, инструкция.<br>Внимание! Общая инструкция на русском языке    выдается отдельно по запросу. <br>Полного перевода набора нет.</div>
    */

    /*
    <div id="itemfeatures" aria-labelledby="ui-id-2" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" aria-hidden="true" style="display: none;">
    <div class="featureline"><dt class="featuretitle">Артикул:</dt><dd class="featurefield">16633</dd></div><div class="featureline"><dt class="featuretitle">Марка:</dt><dd class="featurefield">"DIMENSIONS"</dd></div><div class="featureline"><dt class="featuretitle">Назначение:</dt><dd class="featurefield">для вышивания</dd></div><div class="featureline"><dt class="featuretitle">Объем единицы продажи, л:</dt><dd class="featurefield">.66</dd></div><div class="featureline"><dt class="featuretitle">Страна происхождения:</dt><dd class="featurefield">Китай</dd></div><div class="featureline"><dt class="featuretitle">Тип товара:</dt><dd class="featurefield">Набор</dd></div><div class="featureline"><dt class="featuretitle">Заполнение:</dt><dd class="featurefield">Частичное</dd></div><div class="featureline"><dt class="featuretitle">Схема:</dt><dd class="featurefield">Цветная</dd></div><div class="featureline"><dt class="featuretitle">Тема:</dt><dd class="featurefield">Детские сюжеты</dd></div><div class="featureline"><dt class="featuretitle">Техника исполнения:</dt><dd class="featurefield">Счетный крест</dd></div><div class="featureline"><dt class="featuretitle">Тип канвы/ткани:</dt><dd class="featurefield">Aida 14</dd></div><div class="featureline"><dt class="featuretitle">Цвет канвы/ткани:</dt><dd class="featurefield">белый</dd></div><div class="featureline"><dt class="featuretitle">Размер готовой работы:</dt><dd class="featurefield">18 x 13 см</dd></div></div>
    */

    public static String parseDescription(String elementHtmlCode) {
        String value = findValue(elementHtmlCode, ">", "</div>");
        value = value.replaceAll("<(/?[^>]+)>|\n", " ").trim().replaceAll(" +", " ");
        return value;
    }


    public static String parseSpecifications(Element elementHtmlCode) {
        String value = "";
        Elements features = elementHtmlCode.select("div.featureline");
        for (Element f : features) {
            String featuretitle = f.getElementsByClass("featuretitle").first().text();
            String featurefield = f.getElementsByClass("featurefield").first().text();
            value += featuretitle + " " + featurefield + ". ";
        }
        value = value.replaceAll("<(/?[^>]+)>|\n", " ").trim().replaceAll(" +", " ");
        return value;
    }
}

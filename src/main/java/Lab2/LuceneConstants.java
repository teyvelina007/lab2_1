package Lab2;

public class LuceneConstants {
    public static final String URL = "embroidery_url";
    public static final String IMAGE = "embroidery_image";
    public static final String TITLE = "embroidery_title";
    public static final String PRICE = "embroidery_price";
    public static final String DESCRIPTION = "embroidery_description";
    public static final String SPECIFICATIONS = "embroidery_specifications";
    public static final String TYPE = "embroidery_type";
}

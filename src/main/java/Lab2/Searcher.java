package Lab2;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Searcher {
    public ArrayList<Embroidery> search(String constant, String text, String indexDirectoryPath) throws IOException, ParseException {
        Directory indexDirectory = FSDirectory.open(new File(indexDirectoryPath));
        IndexReader indexReader = DirectoryReader.open(indexDirectory);
        IndexSearcher indexSearcher = new IndexSearcher(indexReader);

        QueryParser queryParser = new QueryParser(Version.LUCENE_40, constant, new StandardAnalyzer(Version.LUCENE_40));
        Query query = queryParser.parse(text);
        System.out.println("Search... '" + query + "'");

        TopDocs docs = indexSearcher.search(query, 10000);

        long num = docs.totalHits;
        System.out.println("Number of results: " + num + '\n');

        ArrayList<Embroidery> searchResult = new ArrayList<>();

        for(ScoreDoc scoreDoc : docs.scoreDocs){
            Document doc = indexSearcher.doc(scoreDoc.doc);
            Embroidery embroidery = new Embroidery(
                    doc.get(LuceneConstants.URL),
                    doc.get(LuceneConstants.IMAGE),
                    doc.get(LuceneConstants.TITLE),
                    Integer.parseInt(doc.get(LuceneConstants.PRICE)),
                    doc.get(LuceneConstants.DESCRIPTION),
                    doc.get(LuceneConstants.SPECIFICATIONS),
                    doc.get(LuceneConstants.TYPE));
            searchResult.add(embroidery);
            // add String
            //System.out.println(embroidery.toSimpleString());
        }
        return searchResult;
    }
}

package Lab2;

public class Embroidery {

    private String embroidery_url;
    private String embroidery_image;
    private String embroidery_title;
    private Integer embroidery_price;
    private String embroidery_description;
    private String embroidery_specifications;
    private String embroidery_type;

    public Embroidery(){}

    public Embroidery(String embroidery_url, String embroidery_image, String embroidery_title, Integer embroidery_price,
                      String embroidery_description, String embroidery_specifications, String embroidery_type){
        this.embroidery_url = embroidery_url;
        this.embroidery_image = embroidery_image;
        this.embroidery_title = embroidery_title;
        this.embroidery_price = embroidery_price;
        this.embroidery_description = embroidery_description;
        this.embroidery_specifications = embroidery_specifications;
        this.embroidery_type = embroidery_type;
    }

    public void setEmbroidery_url(String embroidery_url) {
        this.embroidery_url = embroidery_url;
    }

    public void setEmbroidery_image(String embroidery_image) {
        this.embroidery_image = embroidery_image;
    }

    public void setEmbroidery_title(String embroidery_title) {
        this.embroidery_title = embroidery_title;
    }

    public void setEmbroidery_price(Integer embroidery_price) {
        this.embroidery_price = embroidery_price;
    }

    public void setEmbroidery_description(String embroidery_description) {
        this.embroidery_description = embroidery_description;
    }

    public void setEmbroidery_specifications(String embroidery_specifications) {
        this.embroidery_specifications = embroidery_specifications;
    }

    public void setEmbroidery_type(String embroidery_type) {
        this.embroidery_type = embroidery_type;
    }

    public String getEmbroidery_url() {
        return embroidery_url;
    }

    public String getEmbroidery_image() {
        return embroidery_image;
    }

    public String getEmbroidery_title() {
        return embroidery_title;
    }

    public Integer getEmbroidery_price() {
        return embroidery_price;
    }

    public String getEmbroidery_description() {
        return embroidery_description;
    }

    public String getEmbroidery_specifications() {
        return embroidery_specifications;
    }

    public String getEmbroidery_type() { return embroidery_type; }

    @Override
    public String toString() {
        return "embroidery_url = " + embroidery_url + "\n"
                + "embroidery_image = " + embroidery_image + "\n"
                + "embroidery_title = " + embroidery_title + "\n"
                + "embroidery_price = " + embroidery_price + "\n"
                + "embroidery_description = " + embroidery_description + "\n"
                + "embroidery_specifications = " + embroidery_specifications + "\n"
                + "embroidery_type = " + embroidery_type + "\n";
    }

    public String toSimpleString() {
        return embroidery_url + "\n" + embroidery_image + "\n" + embroidery_title + "\n" + embroidery_price + "\n"
                + embroidery_description + "\n" + embroidery_specifications + "\n" + embroidery_type + "\n";
    }

    public String toJson() {
        return "{\n"
                + "\"embroidery_url\" = \"" + embroidery_url + "\",\n"
                + "\"embroidery_image\" = \"" + embroidery_image + "\",\n"
                + "\"embroidery_title\" = \"" + embroidery_title + "\",\n"
                + "\"embroidery_price\" = \"" + embroidery_price + "\",\n"
                + "\"embroidery_description\" = \"" + embroidery_description + "\",\n"
                + "\"embroidery_specifications\" = \"" + embroidery_specifications + "\",\n"
                + "\"embroidery_type\" = \"" + embroidery_type + "\",\n"
                + "},\n";
    }
}

package Lab2;

import org.apache.lucene.queryparser.classic.ParseException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


public class Main {
    private static int ErrorCount = 0;
    private static int counter = 1;
    private static String MAINURL = "https://leonardohobby.ru";
    private static String indexDirectoryPath = "/home/teyvelina/IdeaProjects/Lab2_1/index";
    //"C:\\Users\\usss\\IdeaProjects\\Lab2_1\\index";
    private static final ArrayList<Embroidery> list = new ArrayList<Embroidery>();

    private static void loadAll() {
        System.out.println("Data is loading, please wait.");
        long timestart = System.currentTimeMillis();
        String url1 = MAINURL + "/ishop/tree_1436912262/";
        ;
        loadtype(url1, "Вышивка", 161);

        String url2 = MAINURL + "/ishop/tree_1437375562/";
        loadtype(url2, "Канва", 31);

        long timeend = System.currentTimeMillis();
        System.out.println("Времени на загрузку с сайта затрачено: " + ((timeend - timestart) / 1000) + " секунд");
        System.out.println("Errors: " + ErrorCount);
    }

/*
<div class="col col-xs-12 col-sm-4 item gooditem">
  <div class="image">
    <a class="link-on-product" href="/ishop/good_827496901/">
	    <span class="statusrow"></span><img class="lasyloading" newsrc="https://images.firma-gamma.ru/208x208/f/a/d827496901l.jpg" src="https://images.firma-gamma.ru/208x208/f/a/d827496901l.jpg"></a>
  </div>
  <div class="bottom">
    <div class="title">
      <a class="ci_caritemtitle" href="/ishop/good_827496901/">"DIMENSIONS" наборы для вышивания 16633 "Почти идеальный" 18 x 13 см</a>
    </div>

    <span class="hp_caritemprice">990<sup>00</sup><!-- <sup>00</sup> --> руб </span>
    <div class="buttons clearfix">
      <a href="/ishop/good_827496901/" class="button button-orange">Посмотреть</a><a href="#" class="button quickcart" style="margin-left:5px;" data-id="827496901" data-market-source="1001" data-market-type="0" data-market-id="9538924525"><img src="/css/images/cartwicon.png" alt=""></a>
    </div>
  </div>
</div>
*/

    private static void loadtype(String url, String type, int numOfPages) {
        for (int i = 1; i <= numOfPages; i++) {
            loadPage(url, i, type);
        }
    }

    private static void loadPage(String typeUrl, int page, String type) {
        Document document = null;
        try {
            document = Jsoup.connect(typeUrl + "?pages=" + page).userAgent("Yandex").get();
            Elements elements = document.select("div.col.col-xs-12.col-sm-4.item.gooditem");
            if (elements != null) {
                for (Element element : elements) {
                    String elementHtmlCode = element.toString();
                    String embroideryUrl = Parser.parseEmbroideryUrl
                            (elementHtmlCode, "link-on-product\" href=\"", MAINURL);
                    String imageUrl = Parser.parseImageUrl(elementHtmlCode, "newsrc=\"");
                    String title = Parser.findValue(elementHtmlCode, "\"ci_caritemtitle\" href=\"" +
                            embroideryUrl.substring(MAINURL.length()) + "\">", "<");
                    int price = 0;
                    try {
                        price = Integer.parseInt(
                                Parser.findValue(elementHtmlCode, "_caritemprice\">", "<"));
                    } catch (NumberFormatException e) {
                        ErrorCount++;
                        e.printStackTrace();
                    }
                    String description = "-";
                    String specifications = "-";

                    // переходим по ссылке на товар, чтобы считать описание и характеристики
                    Document doc1 = null;
                    try {
                        doc1 = Jsoup.connect(embroideryUrl).userAgent("Yandex").get();
                        try {
                            Element embroideryDescription = doc1.getElementById("itemdesc");
                            description = Parser.parseDescription(embroideryDescription.toString());
                        } catch (Exception e) {
                            ErrorCount++;
                            e.printStackTrace();
                        }
                        try {
                            Element embroiderySpecifications = doc1.getElementById("itemfeatures");
                            specifications = Parser.parseSpecifications(embroiderySpecifications);
                        } catch (Exception e) {
                            ErrorCount++;
                            e.printStackTrace();
                        }
                    } catch (IOException ioe) {
                        ErrorCount++;
                        ioe.printStackTrace();
                    }
                    Embroidery embroidery = new Embroidery(embroideryUrl, imageUrl, title, price, description, specifications, type);
                    list.add(embroidery);

                    System.out.println(counter);
                    counter++;
                    System.out.println(embroidery.toString());
                }
            }
        } catch (IOException ioe) {
            ErrorCount++;
            ioe.printStackTrace();
        }
    }

    private static void dataToFile(String fileName, ArrayList<Embroidery> list) throws IOException {
        FileWriter fileWriter = new FileWriter(fileName);
        System.out.print("Всего элементов: ");
        System.out.println(list.size());
        for (Embroidery embroidery : list) {
            fileWriter.write(embroidery.toSimpleString());
        }
        fileWriter.close();
    }

    private static void dataFromFile(String fileName) throws IOException {
        list.clear();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
        String urlStr = bufferedReader.readLine();
        while (urlStr != null) {
            String imageurlStr = bufferedReader.readLine(),
                    titleStr = bufferedReader.readLine(),
                    priceStr = bufferedReader.readLine(),
                    descriptionStr = bufferedReader.readLine(),
                    specificationsStr = bufferedReader.readLine(),
                    typeStr = bufferedReader.readLine();
            Integer priceInt = Integer.parseInt(priceStr);
            list.add(new Embroidery(urlStr, imageurlStr, titleStr, priceInt, descriptionStr, specificationsStr, typeStr));
            urlStr = bufferedReader.readLine();
        }

    }

    public static void main(String[] args) throws IOException, ParseException {
        list.clear();
        /*//ПАРСИНГ
        loadAll();//загрузка данных в память
        dataToFile("embroideries.txt", list);//запись в файл
        System.out.println(list.size() + " элементов");//1148sec, errors:61*/
        dataFromFile("embroideries.txt");//извлечение данных из файла для индексации
        Indexer indexer = new Indexer(indexDirectoryPath);
        indexer.createIndex(list); //создание индекса
        /*System.out.println("LIST...");//проверка содержимого индекса
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }*/
        Searcher searcher = new Searcher();
        ArrayList <Embroidery> resultsForTypeSearch = searcher.search("embroidery_type","Канва", indexDirectoryPath);
        dataToFile("type_search.txt", resultsForTypeSearch);
    }
}
